/* glass-background-monitor.h  Desktop image monitoring for GNOME
 * Copyright (c) 2002, 2003 Ian McKellar <yakk@yakk.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GLASS_BACKGROUND_MONITOR_H
#define GLASS_BACKGROUND_MONITOR_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#define GLASS_TYPE_BACKGROUND_MONITOR              (glass_background_monitor_get_type ())
#define GLASS_BACKGROUND_MONITOR(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), GLASS_TYPE_BACKGROUND_MONITOR, GlassBackgroundMonitor))
#define GLASS_BACKGROUND_MONITOR_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GLASS_TYPE_BACKGROUND_MONITOR, GlassBackgroundMonitorClass))
#define GLASS_IS_BACKGROUND_MONITOR(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), GLASS_TYPE_BACKGROUND_MONITOR))
#define GLASS_IS_BACKGROUND_MONITOR_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), GLASS_TYPE_BACKGROUND_MONITOR))
#define GLASS_BACKGROUND_MONITOR_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GLASS_TYPE_BACKGROUND_MONITOR, GlassBackgroundMonitorClass))

typedef struct _GlassBackgroundMonitorClass GlassBackgroundMonitorClass;
typedef struct _GlassBackgroundMonitor GlassBackgroundMonitor;

GType			glass_background_monitor_get_type 			(void);
GlassBackgroundMonitor*	glass_background_monitor_new			();
GdkPixbuf * 		glass_background_monitor_get_region (GlassBackgroundMonitor *monitor, int x, int y, int width, int height);
GdkPixbuf * 		glass_background_monitor_get_widget_background (GlassBackgroundMonitor *monitor, GtkWidget *widget);
#endif /* GLASS_BACKGROUND_MONITOR_H */
