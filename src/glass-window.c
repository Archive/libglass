/* glass-window.c  Translucent window hack
 * Copyright (c) 2003 Ian McKellar <yakk@yakk.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include "glass-background-monitor.h"
#include "glass-window.h"

/*
 * FIXME:
 * For a whole host of reasons this won't work in a multi-display environment.
 * If you have a multi-display environment then you should get all enthused
 * about making per-display background monitors, adding the monitor and
 * configure callback in realise (and removing them in unrealise) and so on.
 */

struct _GlassWindow
{
	GtkWindow window;

	gulong monitor_handler_id;
	gulong configure_handler_id;
	gulong press_handler_id;
	gulong release_handler_id;
	gulong move_handler_id;
	guint idle_source_id;

	gdouble last_x;
	gdouble last_y;
};

struct _GlassWindowClass
{
	GtkWindowClass parent_class;
};

static GlassBackgroundMonitor* glass_window_background_monitor = NULL;

static gboolean
glass_window_idle_cb (GlassWindow *window)
{
	glass_update_widget_background (GTK_WIDGET (window),
			glass_window_background_monitor);
	window->idle_source_id = 0;
	return FALSE;
}

static gboolean
glass_window_setup_idle (GlassWindow *window)
{
	if (window->idle_source_id == 0) {
		window->idle_source_id = gtk_idle_add_priority (G_PRIORITY_LOW, 
				(GtkFunction)glass_window_idle_cb, window);
	}
}

static void
glass_window_bg_change_cb (GlassBackgroundMonitor *monitor, gpointer user_data)
{
	glass_window_setup_idle (GLASS_WINDOW (user_data));
}

static gboolean
glass_window_configure_cb (GtkWidget *window, GdkEventConfigure *event, 
		gpointer user_data)
{
	glass_window_setup_idle (GLASS_WINDOW (window));
	return TRUE;
}

static gboolean
glass_window_press_cb (GtkWindow *window, GdkEventButton *event, 
		gpointer user_data)
{
	if (event->button == 2) {
		gtk_window_begin_move_drag (window, event->button,
				event->x_root, event->y_root, event->time);
	}
	return TRUE;
}

static void
glass_window_init (GlassWindow *window)
{
	if (glass_window_background_monitor == NULL) {
		/* FIXME: make this threadsafe or something */
		glass_window_background_monitor =
			glass_background_monitor_new ();
	}
	window->monitor_handler_id = g_signal_connect 
		(G_OBJECT (glass_window_background_monitor), "changed",
			(GCallback) glass_window_bg_change_cb, window);
	window->configure_handler_id = g_signal_connect (G_OBJECT (window), 
			"configure-event",
			(GCallback) glass_window_configure_cb, NULL);
	window->press_handler_id = g_signal_connect (G_OBJECT (window),
			"button-press-event", glass_window_press_cb, NULL);
	gtk_widget_add_events (GTK_WINDOW (window), GDK_BUTTON_PRESS_MASK);

	window->idle_source_id = 0;

	gtk_window_set_decorated (GTK_WINDOW (window), FALSE);
	/*
	gtk_window_set_skip_taskbar_hint (GTK_WINDOW (window), FALSE);
	gtk_window_set_skip_pager_hint (GTK_WINDOW (window), FALSE);
	*/
}

static void
glass_window_finalize (GlassWindow *window)
{
	g_signal_handler_disconnect (window, window->configure_handler_id);
	g_signal_handler_disconnect (glass_window_background_monitor, 
			window->monitor_handler_id);
	if (window->idle_source_id != 0) {
		gtk_idle_remove (window->idle_source_id);
		window->idle_source_id = 0;
	}
}

static void
glass_window_class_init (GlassWindowClass *class)
{
	GObjectClass *gobject_class;
	gobject_class = G_OBJECT_CLASS (class);
	gobject_class->finalize = glass_window_finalize;
}

GType
glass_window_get_type (void)
{
	static GType window_type = 0;

	if (!window_type) {
		static const GTypeInfo window_info = {
			sizeof (GlassWindowClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			(GClassInitFunc) glass_window_class_init,
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (GlassWindow),
			0,		/* n_preallocs */
			(GInstanceInitFunc) glass_window_init,
		};

		window_type = g_type_register_static (GTK_TYPE_WINDOW,
				"GlassWindow", &window_info, 0);
	}

	return window_type;
}


GtkWidget*
glass_window_new (void)
{
	  return g_object_new (GLASS_TYPE_WINDOW, NULL);
}

