/* glass-utils.c  Utility functions for libglass
 * Copyright (c) 2003 Ian McKellar <yakk@yakk.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtkcontainer.h>
#include "glass-utils.h"

GdkPixmap*
glass_tint_pixbuf (const GdkPixbuf *pixbuf, GlassTintType tint) {
	GdkPixmap *pixmap;
	GdkBitmap *bitmap;

	if (tint == GLASS_TINT_LIGHTEN) {
		GdkPixbuf *pixbuf_tinted;
		pixbuf_tinted = gdk_pixbuf_composite_color_simple (pixbuf,
				gdk_pixbuf_get_width (pixbuf),
				gdk_pixbuf_get_height (pixbuf),
				GDK_INTERP_TILES, 128, 256,
				0xFFFFFF, 0xFFFFFF);
		gdk_pixbuf_render_pixmap_and_mask(pixbuf_tinted, &pixmap, 
				&bitmap, 0);
		gdk_pixbuf_unref (pixbuf_tinted);
	} else if (tint == GLASS_TINT_DARKEN) {
		GdkPixbuf *pixbuf_tinted;
		pixbuf_tinted = gdk_pixbuf_composite_color_simple (pixbuf,
				gdk_pixbuf_get_width (pixbuf),
				gdk_pixbuf_get_height (pixbuf),
				GDK_INTERP_TILES, 128, 256,
				0x000000, 0x000000);
		gdk_pixbuf_render_pixmap_and_mask(pixbuf_tinted, &pixmap, 
				&bitmap, 0);
		gdk_pixbuf_unref (pixbuf_tinted);
	} else {
		gdk_pixbuf_render_pixmap_and_mask (pixbuf, &pixmap, &bitmap, 0);
	}
	if (bitmap != NULL) {
		g_object_unref (bitmap);
	}

	return pixmap;
}

static GlassTintType glass_tints[5] = {
	GLASS_TINT_NONE,	/* NORMAL */
	GLASS_TINT_DARKEN,	/* ACTIVE */
	GLASS_TINT_LIGHTEN,	/* PRELIGHT */
	GLASS_TINT_LIGHTEN,	/* SELECTED */
	GLASS_TINT_NONE,	/* INSENSITIVE */
};


struct CallbackState {
	GlassBackgroundMonitor *monitor;
};

static void
glass_update_widget_foreach (GtkWidget *widget, struct CallbackState *cb_state){
	glass_update_widget_background (widget, cb_state->monitor);
}

void
glass_update_widget_background (GtkWidget* widget,
		GlassBackgroundMonitor* monitor) {
	GdkPixbuf* pixbuf;
	GdkPixmap* pixmap;
	GtkStyle* style;
	int i;

	if (monitor == NULL) {
		/* sadness */
		return;
	}

	if (GTK_IS_CONTAINER (widget)) {
		struct CallbackState state;
		state.monitor=monitor;
		gtk_container_forall (GTK_CONTAINER (widget), 
				(GtkCallback)glass_update_widget_foreach,
				&state);
	}

	pixbuf = glass_background_monitor_get_widget_background (monitor, 
			widget);

	style = gtk_style_new ();
	for (i=0; i<5; i++) {
		style->bg_pixmap[i] = glass_tint_pixbuf (pixbuf,
				glass_tints[i]);
	}
	gtk_widget_set_style (widget, style);

	/* FIXME: leaking pixmaps? */
}
