/* glass-window.h  Translucent window hack
 * Copyright (c) 2003 Ian McKellar <yakk@yakk.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GLASS_WINDOW_H__
#define __GLASS_WINDOW_H__


#include <gdk/gdk.h>
#include <gtk/gtkwindow.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GLASS_TYPE_WINDOW                  (glass_window_get_type ())
#define GLASS_WINDOW(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GLASS_TYPE_WINDOW, GlassWindow))
#define GLASS_WINDOW_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GLASS_TYPE_WINDOW, GlassWindowClass))
#define GLASS_IS_WINDOW(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GLASS_TYPE_WINDOW))
#define GLASS_IS_WINDOW_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GLASS_TYPE_WINDOW))
#define GLASS_WINDOW_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GLASS_TYPE_WINDOW, GlassWindowClass))


typedef struct _GlassWindow        GlassWindow;
typedef struct _GlassWindowClass   GlassWindowClass;

GType      glass_window_get_type (void) G_GNUC_CONST;
GtkWidget* glass_window_new      (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GLASS_WINDOW_H__ */
