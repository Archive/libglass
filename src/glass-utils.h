/* glass-utils.h  Utility functions for libglass
 * Copyright (c) 2003 Ian McKellar <yakk@yakk.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GLASS_UTILS_H
#define GLASS_UTILS_H

#include <gtk/gtk.h>

#include "glass-background-monitor.h"

typedef enum {
	GLASS_TINT_NONE,
	GLASS_TINT_LIGHTEN,
	GLASS_TINT_DARKEN,
} GlassTintType;

GdkPixmap*	glass_tint_pixbuf		(const GdkPixbuf *pixbuf, 
						 GlassTintType tint);

void 		glass_update_widget_background	(GtkWidget* widget,
					GlassBackgroundMonitor* monitor);
#endif /* GLASS_UTILS_H */
