/* test.c  Translucent window hack test / example
 * Copyright (c) 2003 Ian McKellar <yakk@yakk.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include "glass-window.h"

int
main (int argc, char **argv) 
{
	GtkWidget *vbox;
	GtkWidget *image;
	GtkWidget *label;
	GtkWidget *button;
	GtkWidget *window;

	gtk_init (&argc, &argv);

	window = glass_window_new ();

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), vbox);

	image = gtk_image_new_from_stock (GTK_STOCK_OK, GTK_ICON_SIZE_DIALOG);
	gtk_container_add (GTK_CONTAINER (vbox), image);
	gtk_widget_show (image);

	button = gtk_button_new_with_label ("hello nasty");
	gtk_widget_show (button);
	gtk_container_add (GTK_CONTAINER (vbox), button);

	label = gtk_label_new ("Hello translucent world");
	gtk_container_add (GTK_CONTAINER (vbox), label);
	gtk_widget_show (label);

	gtk_widget_show (vbox);
	gtk_widget_show (window);

	gtk_main ();

	return 0;
}
